unit interstatic;

{$mode delphi}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, ExtCtrls, ComCtrls, StdCtrls, Spin, StaticInterpolation;

const
  TABLE1D_DIM_X   =   5; // number of elements in X table
  TABLE1D_START_X =   0; // first element of X table
  TABLE1D_STEP_X  =  10; // increase step for each following element
                         // this means that X table is this:                     ( 0, 10, 20, 30, 40)
                         // we also have defined this F(X)=Table1D lookup table: ( 0,  0,  5,  0, 35);
                         // so example interpolation for X=15 gives us result:   F(15)=2.5

  TABLE2D_DIM_X   =   5;
  TABLE2D_START_X =   0;
  TABLE2D_STEP_X  =  10;
  TABLE2D_DIM_Y   =   4;
  TABLE2D_START_Y = -20;
  TABLE2D_STEP_Y  =  10;

  TABLE3D_DIM_X   =   5;
  TABLE3D_START_X =   0;
  TABLE3D_STEP_X  =  10;
  TABLE3D_DIM_Y   =   4;
  TABLE3D_START_Y = -20;
  TABLE3D_STEP_Y  =  10;
  TABLE3D_DIM_Z   =   4;
  TABLE3D_START_Z =   0;
  TABLE3D_STEP_Z  = 100;

  TABLE4D_DIM_X   =   5;
  TABLE4D_START_X =   0;
  TABLE4D_STEP_X  =  10;
  TABLE4D_DIM_Y   =   4;
  TABLE4D_START_Y = -20;
  TABLE4D_STEP_Y  =  10;
  TABLE4D_DIM_Z   =   4;
  TABLE4D_START_Z =   0;
  TABLE4D_STEP_Z  = 100;
  TABLE4D_DIM_U   =   4;
  TABLE4D_START_U =-150;
  TABLE4D_STEP_U  = 100;


  Table1D: packed array[1..TABLE1D_DIM_X] of TInterpolatedType =  ( 0,  0,  5,  0, 35);

  Table2D: packed array[1..TABLE2D_DIM_Y, 1..TABLE2D_DIM_X] of TInterpolatedType =
                                                                 (( 0,  0,  5,  0, 35),
                                                                  ( 0,  5, 15, 55, 75),
                                                                  ( 5, 15, 40, 75, 85),
                                                                  (35, 55, 75,100,100));

  Table3D: packed array[1..TABLE3D_DIM_Z, 1..TABLE3D_DIM_Y, 1..TABLE3D_DIM_X] of TInterpolatedType =
                                                                ((( 0,  0,  5,  0, 35), // z = 0
                                                                  ( 0,  5, 15, 55, 75),
                                                                  ( 5, 15, 40, 75, 85),
                                                                  (35, 55, 75,100,100)),

                                                                 ((-3,  0,  7,  4, 40), // z = 100
                                                                  ( 1,  9, 16, 58, 79),
                                                                  ( 6, 18, 44, 78, 91),
                                                                  (37, 61, 82,120,125)),

                                                                 ((-5, -1, 11,  6, 42), // z = 200
                                                                  ( 3, 12, 19, 65, 84),
                                                                  ( 9, 25, 50, 86, 95),
                                                                  (41, 67, 89,130,150)),

                                                                 ((-9, -3,  7,  4, 33), // z = 300
                                                                  ( 2, 13, 21, 75, 86),
                                                                  (11, 29, 52, 92,111),
                                                                  (45, 74, 93,175,200)));

  Table4D: packed array[1..TABLE4D_DIM_U, 1..TABLE4D_DIM_Z, 1..TABLE4D_DIM_Y, 1..TABLE4D_DIM_X] of TInterpolatedType =
                                                               (((( 0,  0,  5,  0, 35), // u = -150
                                                                  ( 0,  5, 15, 55, 75),
                                                                  ( 5, 15, 40, 75, 85),
                                                                  (35, 55, 75,100,100)),

                                                                 ((-3,  0,  7,  4, 40),
                                                                  ( 1,  9, 16, 58, 79),
                                                                  ( 6, 18, 44, 78, 91),
                                                                  (37, 61, 82,120,125)),

                                                                 ((-5, -1, 11,  6, 42),
                                                                  ( 3, 12, 19, 65, 84),
                                                                  ( 9, 25, 50, 86, 95),
                                                                  (41, 67, 89,130,150)),

                                                                 ((-9, -3,  7,  4, 33),
                                                                  ( 2, 13, 21, 75, 86),
                                                                  (11, 29, 52, 92,111),
                                                                  (45, 74, 93,175,200))),

                                                                ((( 0,  0, 10,  0, 70), // u = -50
                                                                  ( 0, 10, 30,110,150),
                                                                  (10, 30, 80,150,170),
                                                                  (70,110,150,200,200)),

                                                                 ((-6,  0, 14,  8, 80),
                                                                  ( 2, 18, 32,116,158),
                                                                  (12, 36, 88,156,182),
                                                                  (74,122,164,240,250)),

                                                                 ((-10,-2, 22, 12, 84),
                                                                  ( 6, 24, 38,130,168),
                                                                  (18, 50,100,172,190),
                                                                  (82,134,178,260,300)),

                                                                 ((-18,-6, 14,  8, 66),
                                                                  ( 4, 26, 42,150,172),
                                                                  (22, 58,104,184,222),
                                                                  (90,148,186,350,400))),

                                                                ((( 0,  0, 20,  0,140), // u = 50
                                                                  ( 0, 20, 60,220,300),
                                                                  (20, 60,160,300,340),
                                                                 (140,220,300,400,400)),

                                                                 ((-12, 0, 28, 16,160),
                                                                  ( 4, 36, 64,232,316),
                                                                  (24, 72,176,312,364),
                                                                 (148,244,328,480,500)),

                                                                 ((-20,-4, 44, 24,168),
                                                                  (12, 48, 76,260,336),
                                                                  (36,100,200,344,380),
                                                                 (164,268,356,520,600)),

                                                                 ((-36,-12,28, 16,132),
                                                                  ( 8, 52, 84,300,344),
                                                                  (44,116,208,368,444),
                                                                 (180,296,372,700,800))),

                                                                ((( 0,  0,  5,  0, 35), // u = 150
                                                                  ( 0,  5, 15, 55, 75),
                                                                  ( 5, 15, 40, 75, 85),
                                                                  (35, 55, 75,100,100)),

                                                                 ((-3,  0,  7,  4, 40),
                                                                  ( 1,  9, 16, 58, 79),
                                                                  ( 6, 18, 44, 78, 91),
                                                                  (37, 61, 82,120,125)),

                                                                 ((-5, -1, 11,  6, 42),
                                                                  ( 3, 12, 19, 65, 84),
                                                                  ( 9, 25, 50, 86, 95),
                                                                  (41, 67, 89,130,150)),

                                                                 ((-9, -3,  7,  4, 33),
                                                                  ( 2, 13, 21, 75, 86),
                                                                  (11, 29, 52, 92,111),
                                                                  (45, 74, 93,175,200))));


type

  { TForm1 }

  TForm1 = class(TForm)
    Calc4D: TButton;
    Calc3D: TButton;
    Calc1D: TButton;
    Calc2D: TButton;
    Label1: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Query1Dx: TFloatSpinEdit;
    Memo1: TMemo;
    Panel1: TPanel;
    Query2Dx: TFloatSpinEdit;
    Query3Dx: TFloatSpinEdit;
    Query2Dy: TFloatSpinEdit;
    Query4Dx: TFloatSpinEdit;
    Query3Dy: TFloatSpinEdit;
    Query4Dy: TFloatSpinEdit;
    Query3Dz: TFloatSpinEdit;
    Query4Du: TFloatSpinEdit;
    Query4Dz: TFloatSpinEdit;
    StatusBar1: TStatusBar;
    procedure Calc1DClick(Sender: TObject);
    procedure Calc2DClick(Sender: TObject);
    procedure Calc3DClick(Sender: TObject);
    procedure Calc4DClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private

  public

  end;

var
  Form1: TForm1;

implementation

{$R *.lfm}

{ TForm1 }

procedure TForm1.Calc1DClick(Sender: TObject);
var
  QueryResult: TInterpolatedType;
begin
  QueryResult := Interpolate1D(Query1Dx.Value,
                               @Table1D,
                               TABLE1D_DIM_X, TABLE1D_START_X, TABLE1D_STEP_X);
  Memo1.Append('Interpolated 1D:   F(' + Query1Dx.Value.ToString + ')=' + QueryResult.ToString);
end;

procedure TForm1.Calc2DClick(Sender: TObject);
var
  QueryResult: TInterpolatedType;
begin
  QueryResult := Interpolate2D(Query2Dx.Value, Query2Dy.Value,
                               @Table2D,
                               TABLE2D_DIM_X, TABLE2D_START_X, TABLE2D_STEP_X,
                               TABLE2D_DIM_Y, TABLE2D_START_Y, TABLE2D_STEP_Y);
  Memo1.Append('Interpolated 2D:   F(' + Query2Dx.Value.ToString + ',' + Query2Dy.Value.ToString + ')=' + QueryResult.ToString);
end;

procedure TForm1.Calc3DClick(Sender: TObject);
var
  QueryResult: TInterpolatedType;
begin
  QueryResult := Interpolate3D(Query3Dx.Value, Query3Dy.Value, Query3Dz.Value,
                               @Table3D,
                               TABLE3D_DIM_X, TABLE3D_START_X, TABLE3D_STEP_X,
                               TABLE3D_DIM_Y, TABLE3D_START_Y, TABLE3D_STEP_Y,
                               TABLE3D_DIM_Z, TABLE3D_START_Z, TABLE3D_STEP_Z);
  Memo1.Append('Interpolated 3D:   F(' + Query3Dx.Value.ToString + ','  + Query3Dy.Value.ToString + ',' + Query3Dz.Value.ToString + ')=' + QueryResult.ToString);
end;

procedure TForm1.Calc4DClick(Sender: TObject);
var
  QueryResult: TInterpolatedType;
begin
  QueryResult := Interpolate4D(Query4Dx.Value, Query4Dy.Value, Query4Dz.Value, Query4Du.Value,
                               @Table4D,
                               TABLE4D_DIM_X, TABLE4D_START_X, TABLE4D_STEP_X,
                               TABLE4D_DIM_Y, TABLE4D_START_Y, TABLE4D_STEP_Y,
                               TABLE4D_DIM_Z, TABLE4D_START_Z, TABLE4D_STEP_Z,
                               TABLE4D_DIM_U, TABLE4D_START_U, TABLE4D_STEP_U);
  Memo1.Append('Interpolated 4D:   F(' + Query4Dx.Value.ToString + ','  + Query4Dy.Value.ToString + ',' + Query4Dz.Value.ToString + ',' + Query4Du.Value.ToString + ')=' + QueryResult.ToString);
end;

procedure TForm1.FormShow(Sender: TObject);
const
  tab   = '          ';
  steps = '   steps: ';
var
  s: string;
  i: word;
  num: TInterpolatedType;
begin
  // 1D table:
  s := '';
  for num in Table1D do
    s := s + num.ToString + ', ';
  Memo1.Append('1D table: ' + s + LineEnding);

  s := tab;
  for i := 1 to TABLE1D_DIM_X do
    s := s + 'x' + i.ToString + '=' + (TABLE1D_START_X + (i-1) * TABLE1D_STEP_X).ToString + ', ';
  Memo1.Append(s + LineEnding);

  // 2D table:
  s := '';
  for i := 0 to TABLE2D_DIM_Y * TABLE2D_DIM_X - 1 do
  begin
    s := s + (PInterpolatedType(@Table2D) + i)^.ToString + ', ';
    if (((i+1) mod TABLE2D_DIM_X) = 0) and (i <> (TABLE2D_DIM_Y * TABLE2D_DIM_X - 1)) then
      s := s + LineEnding + tab;
  end;
  Memo1.Append('2D table: ' + s + LineEnding);

  s := steps;
  for i := 1 to TABLE2D_DIM_X do
    s := s + 'x' + i.ToString + '=' + (TABLE2D_START_X + (i-1) * TABLE2D_STEP_X).ToString + ', ';
  Memo1.Append(s);
  s := tab;
  for i := 1 to TABLE2D_DIM_Y do
    s := s + 'y' + i.ToString + '=' + (TABLE2D_START_Y + (i-1) * TABLE2D_STEP_Y).ToString + ', ';
  Memo1.Append(s + LineEnding);

  // 3D table:
  s := '';
  for i := 0 to TABLE3D_DIM_Z * TABLE3D_DIM_Y * TABLE3D_DIM_X - 1 do
  begin
    s := s + (PInterpolatedType(@Table3D) + i)^.ToString + ', ';
    if (((i+1) mod TABLE3D_DIM_X) = 0) and (i <> (TABLE3D_DIM_Z * TABLE3D_DIM_Y * TABLE3D_DIM_X - 1)) then
      s := s + LineEnding + tab;
    if (((i+1) mod (TABLE3D_DIM_X * TABLE3D_DIM_Y)) = 0) and (i <> (TABLE3D_DIM_Z * TABLE3D_DIM_Y * TABLE3D_DIM_X - 1)) then
      s := s + LineEnding + tab;
  end;
  Memo1.Append('3D table: ' + s + LineEnding);

  s := steps;
  for i := 1 to TABLE3D_DIM_X do
    s := s + 'x' + i.ToString + '=' + (TABLE3D_START_X + (i-1) * TABLE3D_STEP_X).ToString + ', ';
  Memo1.Append(s);
  s := tab;
  for i := 1 to TABLE3D_DIM_Y do
    s := s + 'y' + i.ToString + '=' + (TABLE3D_START_Y + (i-1) * TABLE3D_STEP_Y).ToString + ', ';
  Memo1.Append(s);
  s := tab;
  for i := 1 to TABLE3D_DIM_Z do
    s := s + 'z' + i.ToString + '=' + (TABLE3D_START_Z + (i-1) * TABLE3D_STEP_Z).ToString + ', ';
  Memo1.Append(s + LineEnding);

  // 3D table:
  Memo1.Append('4D table: Too big to show here, so look into the code...' + LineEnding);
end;

end.

