unit interdynamic;

{$mode delphi}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, ExtCtrls, ComCtrls, StdCtrls, Spin, DynamicInterpolation;

const
  TABLE1D_START_X =   0; // first element of X table
  TABLE1D_STEP_X  =  10; // increase step for each following element
                         // this means that X table is this:                     ( 0, 10, 20, 30, 40)
                         // we also have defined this F(X)=Table1D lookup table: ( 0,  0,  5,  0, 35);
                         // so example interpolation for X=15 gives us result:   F(15)=2.5

  TABLE2D_START_X =   0;
  TABLE2D_STEP_X  =  10;
  TABLE2D_START_Y = -20;
  TABLE2D_STEP_Y  =  10;

  TABLE3D_START_X =   0;
  TABLE3D_STEP_X  =  10;
  TABLE3D_START_Y = -20;
  TABLE3D_STEP_Y  =  10;
  TABLE3D_START_Z =   0;
  TABLE3D_STEP_Z  = 100;

  TABLE4D_START_X =   0;
  TABLE4D_STEP_X  =  10;
  TABLE4D_START_Y = -20;
  TABLE4D_STEP_Y  =  10;
  TABLE4D_START_Z =   0;
  TABLE4D_STEP_Z  = 100;
  TABLE4D_START_U =-150;
  TABLE4D_STEP_U  = 100;

type

  { TForm1 }

  TForm1 = class(TForm)
    Calc4D: TButton;
    Calc3D: TButton;
    Calc1D: TButton;
    Calc2D: TButton;
    Label1: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Query1Dx: TFloatSpinEdit;
    Memo1: TMemo;
    Panel1: TPanel;
    Query2Dx: TFloatSpinEdit;
    Query3Dx: TFloatSpinEdit;
    Query2Dy: TFloatSpinEdit;
    Query4Dx: TFloatSpinEdit;
    Query3Dy: TFloatSpinEdit;
    Query4Dy: TFloatSpinEdit;
    Query3Dz: TFloatSpinEdit;
    Query4Du: TFloatSpinEdit;
    Query4Dz: TFloatSpinEdit;
    StatusBar1: TStatusBar;
    procedure Calc1DClick(Sender: TObject);
    procedure Calc2DClick(Sender: TObject);
    procedure Calc3DClick(Sender: TObject);
    procedure Calc4DClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private

  public

  end;

var
  Form1: TForm1;
  DynTable1D: TArr1D;
  DynTable2D: TArr2D;
  DynTable3D: TArr3D;
  DynTable4D: TArr4D;
  QueryResult: TInterpolatedType;

implementation

{$R *.lfm}

{ TForm1 }

procedure TForm1.Calc1DClick(Sender: TObject);
begin
  QueryResult := Interpolate1D(Query1Dx.Value,
                               DynTable1D,
                               TABLE1D_START_X, TABLE1D_STEP_X);
  Memo1.Append('Interpolated 1D:   F(' + Query1Dx.Value.ToString + ')=' + QueryResult.ToString);
end;

procedure TForm1.Calc2DClick(Sender: TObject);
var
  QueryResult: TInterpolatedType;
begin
  QueryResult := Interpolate2D(Query2Dx.Value, Query2Dy.Value,
                               DynTable2D,
                               TABLE2D_START_X, TABLE2D_STEP_X,
                               TABLE2D_START_Y, TABLE2D_STEP_Y);
  Memo1.Append('Interpolated 2D:   F(' + Query2Dx.Value.ToString + ',' + Query2Dy.Value.ToString + ')=' + QueryResult.ToString);
end;

procedure TForm1.Calc3DClick(Sender: TObject);
var
  QueryResult: TInterpolatedType;
begin
  QueryResult := Interpolate3D(Query3Dx.Value, Query3Dy.Value, Query3Dz.Value,
                               DynTable3D,
                               TABLE3D_START_X, TABLE3D_STEP_X,
                               TABLE3D_START_Y, TABLE3D_STEP_Y,
                               TABLE3D_START_Z, TABLE3D_STEP_Z);
  Memo1.Append('Interpolated 3D:   F(' + Query3Dx.Value.ToString + ','  + Query3Dy.Value.ToString + ',' + Query3Dz.Value.ToString + ')=' + QueryResult.ToString);
end;

procedure TForm1.Calc4DClick(Sender: TObject);
var
  QueryResult: TInterpolatedType;
begin
  QueryResult := Interpolate4D(Query4Dx.Value, Query4Dy.Value, Query4Dz.Value, Query4Du.Value,
                               DynTable4D,
                               TABLE4D_START_X, TABLE4D_STEP_X,
                               TABLE4D_START_Y, TABLE4D_STEP_Y,
                               TABLE4D_START_Z, TABLE4D_STEP_Z,
                               TABLE4D_START_U, TABLE4D_STEP_U);
  Memo1.Append('Interpolated 4D:   F(' + Query4Dx.Value.ToString + ','  + Query4Dy.Value.ToString + ',' + Query4Dz.Value.ToString + ',' + Query4Du.Value.ToString + ')=' + QueryResult.ToString);
end;

procedure TForm1.FormShow(Sender: TObject);
const
  tab   = '          ';
  steps = '   steps: ';
var
  s: string;
  x, y, z, u, DimX, DimY, DimZ: word;
  num: TInterpolatedType;
begin
  // 1D table:
  DimX := Length(DynTable1D);
  //
  s := '';
  for num in DynTable1D do
    s := s + num.ToString + ', ';
  Memo1.Append('1D table: ' + s + LineEnding);
  //
  s := steps;
  for x := 1 to DimX do
    s := s + 'x' + x.ToString + '=' + (TABLE1D_START_X + (x - 1) * TABLE1D_STEP_X).ToString + ', ';
  Memo1.Append(s + LineEnding);

  // 2D table:
  DimX := Length(DynTable2D[0]);
  DimY := Length(DynTable2D);
  //
  s := '';
  for y := 0 to DimY - 1 do
  begin
    for x := 0 to DimX - 1 do
      s := s + DynTable2D[y, x].ToString + ', ';
    if y <> DimY - 1 then
      s := s + System.LineEnding + tab;
  end;
  Memo1.Append('2D table: ' + s + LineEnding);
  //
  s := steps;
  for x := 1 to DimX do
    s := s + 'x' + x.ToString + '=' + (TABLE2D_START_X + (x-1) * TABLE2D_STEP_X).ToString + ', ';
  Memo1.Append(s);
  //
  s := tab;
  for y := 1 to DimY do
    s := s + 'y' + y.ToString + '=' + (TABLE2D_START_Y + (y-1) * TABLE2D_STEP_Y).ToString + ', ';
  Memo1.Append(s + LineEnding);

  // 3D table:
  DimX := Length(DynTable3D[0, 0]);
  DimY := Length(DynTable3D[0]);
  DimZ := Length(DynTable3D);
  //
  s := '';
  for z := 0 to DimZ - 1 do
  begin
    for y := 0 to DimY - 1 do
    begin
      for x := 0 to DimX - 1 do
        s := s + DynTable3D[z, y, x].ToString + ', ';
      s := s + LineEnding + tab;
    end;
    if z <> DimZ - 1 then
      s := s + LineEnding + tab;
  end;
  Memo1.Append('3D table: ' + s);
  //
  s := steps;
  for x := 1 to DimX do
    s := s + 'x' + x.ToString + '=' + (TABLE3D_START_X + (x-1) * TABLE3D_STEP_X).ToString + ', ';
  Memo1.Append(s);
  //
  s := tab;
  for y := 1 to DimY do
    s := s + 'y' + y.ToString + '=' + (TABLE3D_START_Y + (y-1) * TABLE3D_STEP_Y).ToString + ', ';
  Memo1.Append(s);
  //
  s := tab;
  for z := 1 to DimZ do
    s := s + 'z' + z.ToString + '=' + (TABLE3D_START_Z + (z-1) * TABLE3D_STEP_Z).ToString + ', ';
  Memo1.Append(s + LineEnding);

  // 3D table:
  Memo1.Append('4D table: Too big to show here, so look into the code...' + LineEnding);
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  // 1D:
  DynTable1D  := TArr1D.Create( 0,  0,  5,  0, 35);
  // 1D alternative array init:
  // SetLength(DynTable1D, 5);
  // DynTable1D[0] := 0;  DynTable1D[1] := 0;  DynTable1D[2] := 5;  DynTable1D[3] := 0;  DynTable1D[4] := 35;

  // 2D:
  DynTable2D  := TArr2D.Create(TArr1D.Create( 0,  0,  5,  0, 35),
                               TArr1D.Create( 0,  5, 15, 55, 75),
                               TArr1D.Create( 5, 15, 40, 75, 85),
                               TArr1D.Create(35, 55, 75,100,100));

  // 3D:
  DynTable3D  := TArr3D.Create(TArr2D.Create(TArr1D.Create( 0,  0,  5,  0, 35), // z = 0
                                             TArr1D.Create( 0,  5, 15, 55, 75),
                                             TArr1D.Create( 5, 15, 40, 75, 85),
                                             TArr1D.Create(35, 55, 75,100,100)),

                               TArr2D.Create(TArr1D.Create(-3,  0,  7,  4, 40), // z = 100
                                             TArr1D.Create( 1,  9, 16, 58, 79),
                                             TArr1D.Create( 6, 18, 44, 78, 91),
                                             TArr1D.Create(37, 61, 82,120,125)),

                               TArr2D.Create(TArr1D.Create(-5, -1, 11,  6, 42), // z = 200
                                             TArr1D.Create( 3, 12, 19, 65, 84),
                                             TArr1D.Create( 9, 25, 50, 86, 95),
                                             TArr1D.Create(41, 67, 89,130,150)),

                               TArr2D.Create(TArr1D.Create(-9, -3,  7,  4, 33), // z = 300
                                             TArr1D.Create( 2, 13, 21, 75, 86),
                                             TArr1D.Create(11, 29, 52, 92,111),
                                             TArr1D.Create(45, 74, 93,175,200)));

  // 4D:
  DynTable4D  := TArr4D.Create(TArr3D.Create(TArr2D.Create(TArr1D.Create( 0,  0,  5,  0, 35), // u = -150
                                                           TArr1D.Create( 0,  5, 15, 55, 75),
                                                           TArr1D.Create( 5, 15, 40, 75, 85),
                                                           TArr1D.Create(35, 55, 75,100,100)),

                                             TArr2D.Create(TArr1D.Create(-3,  0,  7,  4, 40),
                                                           TArr1D.Create( 1,  9, 16, 58, 79),
                                                           TArr1D.Create( 6, 18, 44, 78, 91),
                                                           TArr1D.Create(37, 61, 82,120,125)),

                                             TArr2D.Create(TArr1D.Create(-5, -1, 11,  6, 42),
                                                           TArr1D.Create( 3, 12, 19, 65, 84),
                                                           TArr1D.Create( 9, 25, 50, 86, 95),
                                                           TArr1D.Create(41, 67, 89,130,150)),

                                             TArr2D.Create(TArr1D.Create(-9, -3,  7,  4, 33),
                                                           TArr1D.Create( 2, 13, 21, 75, 86),
                                                           TArr1D.Create(11, 29, 52, 92,111),
                                                           TArr1D.Create(45, 74, 93,175,200))),

                               TArr3D.Create(TArr2D.Create(TArr1D.Create( 0,  0, 10,  0, 70), // u = -50
                                                           TArr1D.Create( 0, 10, 30,110,150),
                                                           TArr1D.Create(10, 30, 80,150,170),
                                                           TArr1D.Create(70,110,150,200,200)),

                                             TArr2D.Create(TArr1D.Create(-6,  0, 14,  8, 80),
                                                           TArr1D.Create( 2, 18, 32,116,158),
                                                           TArr1D.Create(12, 36, 88,156,182),
                                                           TArr1D.Create(74,122,164,240,250)),

                                             TArr2D.Create(TArr1D.Create(-10,-2, 22, 12, 84),
                                                           TArr1D.Create( 6, 24, 38,130,168),
                                                           TArr1D.Create(18, 50,100,172,190),
                                                           TArr1D.Create(82,134,178,260,300)),

                                             TArr2D.Create(TArr1D.Create(-18,-6, 14,  8, 66),
                                                           TArr1D.Create( 4, 26, 42,150,172),
                                                           TArr1D.Create(22, 58,104,184,222),
                                                           TArr1D.Create(90,148,186,350,400))),

                               TArr3D.Create(TArr2D.Create(TArr1D.Create( 0,  0, 20,  0,140), // u = 50
                                                           TArr1D.Create( 0, 20, 60,220,300),
                                                           TArr1D.Create(20, 60,160,300,340),
                                                           TArr1D.Create(140,220,300,400,400)),

                                             TArr2D.Create(TArr1D.Create(-12, 0, 28, 16,160),
                                                           TArr1D.Create( 4, 36, 64,232,316),
                                                           TArr1D.Create(24, 72,176,312,364),
                                                           TArr1D.Create(148,244,328,480,500)),

                                             TArr2D.Create(TArr1D.Create(-20,-4, 44, 24,168),
                                                           TArr1D.Create(12, 48, 76,260,336),
                                                           TArr1D.Create(36,100,200,344,380),
                                                           TArr1D.Create(164,268,356,520,600)),

                                             TArr2D.Create(TArr1D.Create(-36,-12,28, 16,132),
                                                           TArr1D.Create( 8, 52, 84,300,344),
                                                           TArr1D.Create(44,116,208,368,444),
                                                           TArr1D.Create(180,296,372,700,800))),

                               TArr3D.Create(TArr2D.Create(TArr1D.Create( 0,  0,  5,  0, 35), // u = 150
                                                           TArr1D.Create( 0,  5, 15, 55, 75),
                                                           TArr1D.Create( 5, 15, 40, 75, 85),
                                                           TArr1D.Create(35, 55, 75,100,100)),

                                             TArr2D.Create(TArr1D.Create(-3,  0,  7,  4, 40),
                                                           TArr1D.Create( 1,  9, 16, 58, 79),
                                                           TArr1D.Create( 6, 18, 44, 78, 91),
                                                           TArr1D.Create(37, 61, 82,120,125)),

                                             TArr2D.Create(TArr1D.Create(-5, -1, 11,  6, 42),
                                                           TArr1D.Create( 3, 12, 19, 65, 84),
                                                           TArr1D.Create( 9, 25, 50, 86, 95),
                                                           TArr1D.Create(41, 67, 89,130,150)),

                                             TArr2D.Create(TArr1D.Create(-9, -3,  7,  4, 33),
                                                           TArr1D.Create( 2, 13, 21, 75, 86),
                                                           TArr1D.Create(11, 29, 52, 92,111),
                                                           TArr1D.Create(45, 74, 93,175,200))));
end;

end.

