unit bedlevelmesh;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, ExtCtrls, ComCtrls,
  StdCtrls, TATools, TASources, TAGraph, TAFuncSeries, TASeries, TALegend, TADrawUtils, DynamicInterpolation;

type

  { TBedLevelMeshMapForm }

  TBedLevelMeshMapForm = class(TForm)
    DevMeas: TLabel;
    DevBed: TLabel;
    MeasurementsGroupBox: TGroupBox;
    BedGroupBox: TGroupBox;
    LoadDataButton: TButton;
    BedLevelChart: TChart;
    BedColorMap: TColorMapSeries;
    BedHeights: TLineSeries;
    BedEdges: TLineSeries;
    ColorMapSource: TListChartSource;
    MaxMeas: TLabel;
    MaxBed: TLabel;
    MinMeas: TLabel;
    MinBed: TLabel;
    Panel1: TPanel;
    Panel2: TPanel;
    StatusBar: TStatusBar;
    procedure BedLevelChartDrawLegend(ASender: TChart; ADrawer: IChartDrawer; ALegendItems: TChartLegendItems; ALegendItemSize: TPoint; const ALegendRect: TRect; AColCount, ARowCount: Integer);
    procedure BedLevelChartMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
    procedure LoadDataButtonClick(Sender: TObject);
    procedure BedColorMapCalculate(const AX, AY: double; out AZ: double);
    procedure FormCreate(Sender: TObject);
  private

  public

  end;

const
  TABLE2D_MIN_X   =   0;
  TABLE2D_MAX_X   = 330;
  TABLE2D_MIN_Y   =   0;
  TABLE2D_MAX_Y   = 330;
  TABLE2D_START_X =  30;
  TABLE2D_STEP_X  = 135;
  TABLE2D_START_Y =  30;
  TABLE2D_STEP_Y  = 135;

var
  BedLevelMeshMapForm: TBedLevelMeshMapForm;
  Table2D: TArr2D;
  MeasuredMinZ, MeasuredMaxZ: double;
  BedMinZ, BedMaxZ: double;
  BedMinMaxChanged: boolean = false;

implementation

{$R *.lfm}

{ TBedLevelMeshMapForm }

procedure TBedLevelMeshMapForm.LoadDataButtonClick(Sender: TObject);
var
  x, y: word;
  BedX, BedY, BedZ: double;
begin
  MeasuredMaxZ := Low(longword);
  MeasuredMinZ := High(longword);
  BedMaxZ      := Low(longword);
  BedMinZ      := High(longword);

  with BedEdges do
  begin
    BeginUpdate;
    Clear;
    AddXY(TABLE2D_MIN_X, TABLE2D_MIN_Y);
    AddXY(TABLE2D_MAX_X, TABLE2D_MIN_Y);
    AddXY(TABLE2D_MAX_X, TABLE2D_MAX_Y);
    AddXY(TABLE2D_MIN_X, TABLE2D_MAX_Y);
    AddXY(TABLE2D_MIN_X, TABLE2D_MIN_Y);
    EndUpdate;
  end;

  Table2D := TArr2D.Create(TArr1D.Create(  0.011, -0.020, -0.026),
                           TArr1D.Create( -0.019, -0.011,  0.017),
                           TArr1D.Create(  0.023, -0.031, -0.022));
  with BedHeights do
  begin
    BeginUpdate;
    for x := 0 to Length(Table2D[0]) - 1 do
      for y := 0 to Length(Table2D) - 1 do
      begin
        BedX := TABLE2D_START_X + x * TABLE2D_STEP_X;
        BedY := TABLE2D_START_Y + y * TABLE2D_STEP_Y;
        BedZ := Table2D[x, y];
        BedHeights.AddXY(BedX, BedY, FloatToStr(BedZ));

        if BedZ > MeasuredMaxZ then
          MeasuredMaxZ := BedZ;

        if BedZ < MeasuredMinZ then
          MeasuredMinZ := BedZ;
      end;
    EndUpdate;
  end;

  MinMeas.Caption := 'Min:   ' + MeasuredMinZ.ToString + ' mm';
  MaxMeas.Caption := 'Max:   ' + MeasuredMaxZ.ToString + ' mm';
  DevMeas.Caption := 'Dev:   ' + Abs((MeasuredMaxZ - MeasuredMinZ)).ToString + ' mm';
end;

procedure TBedLevelMeshMapForm.BedLevelChartMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
var
  BedX, BedY, BedZ: double;
begin
  if BedHeights.IsEmpty then
    Exit;
  BedY := BedLevelChart.XImageToGraph(X); // X and Y are inverted in chart
  BedX := BedLevelChart.YImageToGraph(Y);
  BedZ := Interpolate2D(BedX, BedY,
                        Table2D,
                        TABLE2D_START_X, TABLE2D_STEP_X,
                        TABLE2D_START_Y, TABLE2D_STEP_Y);
  StatusBar.SimpleText := '  X=' + Round(BedX).ToString + '   Y=' + Round(BedY).ToString + '     Z=' + FloatToStrF(BedZ, ffFixed, 6, 3);
end;

procedure TBedLevelMeshMapForm.BedLevelChartDrawLegend(ASender: TChart; ADrawer: IChartDrawer; ALegendItems: TChartLegendItems; ALegendItemSize: TPoint;
                                                       const ALegendRect: TRect; AColCount, ARowCount: Integer);
var
  xg1, xg2, yg1, yg2, y: Integer;
  s: String;
  z, dz, z1, step: Double;
  m, ex: Integer;
  i: Integer;
  ts: TPoint;
  clr: TColor;
begin
  xg1 := ALegendRect.Left + 8;              // left edge of gradient
  xg2 := xg1 + ASender.Legend.SymbolWidth;  // right edge of gradient
  yg1 := ASender.ClipRect.Top + 1;          // top edge of gradient
  yg2 := ASender.ClipRect.Bottom - 1;       // bottom edge of gradient

  WriteLn('yg1=' + yg1.ToString + ' yg2=' + yg2.ToString);

  // Draw black border around gradient bar
  ADrawer.SetPenParams(psSolid, clBlack);
  ADrawer.SetBrushParams(bsClear, clNone);
  ADrawer.Rectangle(xg1-1, yg1 - 1, xg2 + 2, yg2 + 2); //  ADrawer.Rectangle(xg1-1, yg1-1, xg2+1, yg2+1);

  // Draw gradient bar
  ts := ADrawer.TextExtent('1');       // ts.y is the height of the legend font.
  i := 1;
  step := (yg2 - yg1) / 10;
  for y := yg1 to yg2 do     // y runs through the legend bar on the screen
  begin
    // We calculate which z value (i.e. color) belongs to the current height within the bar.
    //z := (yg2 - y) / (yg2 - yg1) * (0.030 - -0.030) + -0.030;
    z := BedMinZ + (y - yg1) / (yg2 - yg1) * (BedMaxZ - BedMinZ);
    //z := (yg2 - y) / (yg2 - yg1);
    //if not BedHeights.IsEmpty then
    //  WriteLn('y='+y.ToString + ' z='+z.ToString);
    //z := (yg2 - y) / (yg2 - yg1) * (MeasuredMaxZ - MeasuredMinZ) + MeasuredMinZ;
    if ((y > (i -1) * step ) and (i <= 10)) or (y = yg2) then
    begin
      ADrawer.SetPenParams(psSolid, clBlack);
      ADrawer.Line(xg2-2, y, xg2+4, y);
      //ADrawer.TextOut.Pos(xg2+12, y{-ts.y div 2}).Text(z.ToString).Done;
      //z1 := -0.030 + (i-1) * 0.01;
      z  := BedMinZ + (y - yg1) / (yg2 - yg1) * (BedMaxZ - BedMinZ);
      z1 := z;
      ADrawer.TextOut.Pos(xg2+12, y-(ts.y div 2)).Text(z1.ToString(ffFixed, 10, 3) + ' mm').Done;
      Inc(i);
      WriteLn('y=' + y.ToString + ' z=' + z.ToString);
    end;
    clr := BedColorMap.ColorByValue(z);
    // Draw a horizontal line in this color at the current height
    ADrawer.SetPenParams(psSolid, clr);
    //ADrawer.SetPenParams(psSolid, clBlack);
    ADrawer.Line(xg1, y, xg2, y);
  end;

  // Draw axis labels along gradient bar, with a short marker line
  ADrawer.SetBrushParams(bsSolid, clBlack);
  ADrawer.SetPenParams(psSolid, clBlack);
  ADrawer.SetFont(ASender.Legend.Font);
  // Calculate decent intervals for the markers
  dz := (MeasuredMaxZ - MeasuredMinZ) / 10;
  str(dz, s);
  m := StrToInt(copy(s, 1, pos('.', s)-1));
  ex := StrToInt(copy(s, pos('E', s)+1, Length(s)));
  if m > 5 then m := 5 else if m > 2 then m := 2 else m := 1;
  dz := StrToFloat(IntToStr(m)+'E'+IntToStr(ex)); // round for decent intervals
  // Calculate the first marker, decently rounded.
  z := trunc(MeasuredMinZ*dz) / dz + dz;
  // Construct all markers by adding dz to the previous value; draw the
  // function value and a short tick line.
  i := 0;
  while z <= MeasuredMaxZ do begin
    Inc(i);
    y := yg2 - round((z - MeasuredMinZ) / (MeasuredMaxZ - MeasuredMinZ) * (yg2 - yg1));
    WriteLn('y=' + y.ToString);
//    ADrawer.Line(xg2-2, y, xg2+4, y);
    //s := {i.ToString + }' y=' + y.ToString + ' z=' + z.ToString;  // s := Format('%.1f%', [z]);
    s := {y.ToString + ' ' +} z.ToString + ' ' + BedColorMap.FunctionValue(z, 0).ToString;  // s := Format('%.1f%', [z]);
//    ADrawer.TextOut.Pos(xg2+12, y-ts.y div 2).Text(s).Done;
    z1 := (yg2 - y) / (yg2 - yg1) * (MeasuredMaxZ - MeasuredMinZ) + MeasuredMinZ;
    WriteLn('z1=' + z1.ToString);
    z := z + dz;
  end;
end;

procedure TBedLevelMeshMapForm.BedColorMapCalculate(const AX, AY: Double; out
  AZ: Double);
begin
  if (AX < BedLevelChart.BottomAxis.Range.Min) or (AX > BedLevelChart.BottomAxis.Range.Max) or (AY < BedLevelChart.LeftAxis.Range.Min) or (AY > BedLevelChart.LeftAxis.Range.Max) or (Table2D = nil) then
    AZ := 99999
  else
  begin
    AZ := Interpolate2D(AX, AY,
                        Table2D,
                        TABLE2D_START_X, TABLE2D_STEP_X,
                        TABLE2D_START_Y, TABLE2D_STEP_Y);
    if BedMaxZ < AZ then
    begin
       BedMaxZ := AZ;
       BedMinMaxChanged := true;
    end;

    if BedMinZ > AZ then
    begin
       BedMinZ := AZ;
       BedMinMaxChanged := true;
    end;
  end;

  if BedMinMaxChanged then
  begin
    BedMinMaxChanged := false;
    MinBed.Caption := 'Min:   ' + BedMinZ.ToString(ffFixed, 10, 3) + ' mm';
    MaxBed.Caption := 'Max:   ' + BedMaxZ.ToString(ffFixed, 10, 3) + ' mm';
    DevBed.Caption := 'Dev:   ' + Abs((BedMaxZ - BedMinZ)).ToString(ffFixed, 10, 3) + ' mm';
  end;
end;

procedure TBedLevelMeshMapForm.FormCreate(Sender: TObject);
begin
  With ColorMapSource do begin // heatmap
    Clear;
    Add(-0.030, 0, '', clRed);     // high negative deviation
    Add(-0.015, 0, '', clYellow);  // medium negative deviation
    Add(+0.000, 0, '', clLime);    // no deviation
    Add(+0.015, 0, '', clYellow);  // medium positive deviation
    Add(+0.030, 0, '', clMaroon);  // high positive deviation
    Add(+99999, 0, '', clGray);    // for anything that is not bed
  end;
  // caption := ColorMapSource.DataPoints.Text;
end;

end.

