# **Interpolate1234D** #



[**Interpolate1234D**](https://bitbucket.org/avra/interpolate1234d/) is a very fast 1, 2, 3 and 4 dimensional [linear interpolation](https://en.wikipedia.org/wiki/Linear_interpolation) library with **O(1)** time complexity supporting both static and dynamic arrays. It is made in an "old school" procedural programming with pointer arithmetic without using any of the modern pascal language features, because it's primary goal was to run on a very simple low power microcontroller and calculate hundreds of fuzzy logic fuzzifications, operations and defuzzifications every second in a real time control loop. The only way to achieve this goal was to avoid intensive calculations and read values for interpolation directly from flash stored static precalculated tables which can have up to 4 dimensions - depending on the number of numerical inputs. On pc you can use both static and dynamic data arrays and allow your self to make big tables with small dimensional steps to achieve great accuracy in all cases. Dimensions multiply each other, so even a small increase in dimensional step density might produce big increase in data size. Secondary goal was to display a z-probe mesh for assisting in manual bed leveling of a 3D printer.

My effort to visually present how multidimensional linear interpolation works can be found [here](https://bitbucket.org/avra/interpolate1234d/downloads/Multidimensional%20Linear%20Interpolation%20Explained.xls). As an interesting side effect this library also handles [linear extrapolation](https://en.wikipedia.org/wiki/Linear_extrapolation) when input values are outside of the table range.




### Screenshots ###
* ![interpolate1234d screenshot](https://bitbucket.org/avra/interpolate1234d/raw/master/ExampleOfInterpolate1234D.png)
* ![3D printer bed level mesh map](https://bitbucket.org/avra/interpolate1234d/raw/master/BedLevel.png)



### Prerequisites

* None.



### Installation

1. The preferred way would be to compile the **interpolate1234d.lpk** package.  That adds **Interpolate1234D** source directory to Lazarus and makes it available to all your projects.  All you have to do is then add **Interpolate1234D** package as a new requirement to your project via **Project / Project Inspector** Lazarus menu.

2. Instead of using a package, you can simply copy **DynamicInterpolation**  and **StaticInterpolation** units into your project directory and simply start using the library.

   

### Using static arrays ###

* **1D** - One dimensional linear interpolation example using static array:

```pascal
uses
  StaticInterpolation;
...
procedure TForm1.Calc1DClick(Sender: TObject);
const
  TABLE1D_DIM_X   =   5; // number of elements in X table
  TABLE1D_START_X =   0; // first element of X table
  TABLE1D_STEP_X  =  10; // increase step for each following element
                         // this means that X table is this:      ( 0, 10, 20, 30, 40)
                         // we also have defined this F(X)=Table1D lookup table:
  Table1D: packed array[1..TABLE1D_DIM_X] of TInterpolatedType =  ( 0,  0,  5,  0, 35);   
                         // so for X=15, interpolation result is F(15)=2.5
var
  QueryResult: TInterpolatedType;
begin
  QueryResult := Interpolate1D(15,
                               @Table1D,
                               TABLE1D_DIM_X, TABLE1D_START_X, TABLE1D_STEP_X);
  Memo1.Append('Interpolated 1D: F(15)=' + QueryResult.ToString);
end;
```

```
  Interpolated 1D: F(15)=2.5
```

* **2D** - Two dimensional linear interpolation example using static arrays:

```pascal
procedure TForm1.Calc2DClick(Sender: TObject);
const
  TABLE2D_DIM_X   =   5;
  TABLE2D_START_X =   0;
  TABLE2D_STEP_X  =  10;
  TABLE2D_DIM_Y   =   4;
  TABLE2D_START_Y = -20;
  TABLE2D_STEP_Y  =  10;    
  Table2D: packed array[1..TABLE2D_DIM_Y, 1..TABLE2D_DIM_X] of TInterpolatedType =
                                                                 (( 0,  0,  5,  0, 35),
                                                                  ( 0,  5, 15, 55, 75),
                                                                  ( 5, 15, 40, 75, 85),
                                                                  (35, 55, 75,100,100));
var
  QueryResult: TInterpolatedType;
begin
  QueryResult := Interpolate2D(18, -17,
                               @Table2D,
                               TABLE2D_DIM_X, TABLE2D_START_X, TABLE2D_STEP_X,
                               TABLE2D_DIM_Y, TABLE2D_START_Y, TABLE2D_STEP_Y);
  Memo1.Append('Interpolated 2D: F(18,-17)=' + QueryResult.ToString);
end; 
```

```
  Interpolated 2D: F(18,-17)=6.7
```

* **3D** - Three dimensional linear interpolation example using static arrays:

```pascal
procedure TForm1.Calc3DClick(Sender: TObject);
const
  TABLE3D_DIM_X   =   5;
  TABLE3D_START_X =   0;
  TABLE3D_STEP_X  =  10;
  TABLE3D_DIM_Y   =   4;
  TABLE3D_START_Y = -20;
  TABLE3D_STEP_Y  =  10;
  TABLE3D_DIM_Z   =   4;
  TABLE3D_START_Z =   0;
  TABLE3D_STEP_Z  = 100;        
  Table3D: packed array[1..TABLE3D_DIM_Z, 1..TABLE3D_DIM_Y, 1..TABLE3D_DIM_X] of TInterpolatedType =
                                                                ((( 0,  0,  5,  0, 35),
                                                                  ( 0,  5, 15, 55, 75),
                                                                  ( 5, 15, 40, 75, 85),
                                                                  (35, 55, 75,100,100)),

                                                                 ((-3,  0,  7,  4, 40),
                                                                  ( 1,  9, 16, 58, 79),
                                                                  ( 6, 18, 44, 78, 91),
                                                                  (37, 61, 82,120,125)),

                                                                 ((-5, -1, 11,  6, 42),
                                                                  ( 3, 12, 19, 65, 84),
                                                                  ( 9, 25, 50, 86, 95),
                                                                  (41, 67, 89,130,150)),

                                                                 ((-9, -3,  7,  4, 33),
                                                                  ( 2, 13, 21, 75, 86),
                                                                  (11, 29, 52, 92,111),
                                                                  (45, 74, 93,175,200)));
var
  QueryResult: TInterpolatedType;
begin
  QueryResult := Interpolate3D(18, -17, 25,
                               @Table3D,
                               TABLE3D_DIM_X, TABLE3D_START_X, TABLE3D_STEP_X,
                               TABLE3D_DIM_Y, TABLE3D_START_Y, TABLE3D_STEP_Y,
                               TABLE3D_DIM_Z, TABLE3D_START_Z, TABLE3D_STEP_Z);
  Memo1.Append('Interpolated 3D: F(18,-17,25)=' + QueryResult.ToString);
end;
```

```
  Interpolated 3D: F(18,-17,25)=7.1
```

* **4D** - Four dimensional linear interpolation example using static arrays:

```pascal
procedure TForm1.Calc4DClick(Sender: TObject);
const
  TABLE4D_DIM_X   =   5;
  TABLE4D_START_X =   0;
  TABLE4D_STEP_X  =  10;
  TABLE4D_DIM_Y   =   4;
  TABLE4D_START_Y = -20;
  TABLE4D_STEP_Y  =  10;
  TABLE4D_DIM_Z   =   4;
  TABLE4D_START_Z =   0;
  TABLE4D_STEP_Z  = 100;
  TABLE4D_DIM_U   =   4;
  TABLE4D_START_U =-150;
  TABLE4D_STEP_U  = 100;
  Table4D: packed array[1..TABLE4D_DIM_U, 1..TABLE4D_DIM_Z, 1..TABLE4D_DIM_Y, 1..TABLE4D_DIM_X] of TInterpolatedType =
                                                               (((( 0,  0,  5,  0, 35),
                                                                  ( 0,  5, 15, 55, 75),
                                                                  ( 5, 15, 40, 75, 85),
                                                                  (35, 55, 75,100,100)),

                                                                 ((-3,  0,  7,  4, 40),
                                                                  ( 1,  9, 16, 58, 79),
                                                                  ( 6, 18, 44, 78, 91),
                                                                  (37, 61, 82,120,125)),

                                                                 ((-5, -1, 11,  6, 42),
                                                                  ( 3, 12, 19, 65, 84),
                                                                  ( 9, 25, 50, 86, 95),
                                                                  (41, 67, 89,130,150)),

                                                                 ((-9, -3,  7,  4, 33),
                                                                  ( 2, 13, 21, 75, 86),
                                                                  (11, 29, 52, 92,111),
                                                                  (45, 74, 93,175,200))),

                                                                ((( 0,  0, 10,  0, 70),
                                                                  ( 0, 10, 30,110,150),
                                                                  (10, 30, 80,150,170),
                                                                  (70,110,150,200,200)),

                                                                 ((-6,  0, 14,  8, 80),
                                                                  ( 2, 18, 32,116,158),
                                                                  (12, 36, 88,156,182),
                                                                  (74,122,164,240,250)),

                                                                 ((-10,-2, 22, 12, 84),
                                                                  ( 6, 24, 38,130,168),
                                                                  (18, 50,100,172,190),
                                                                  (82,134,178,260,300)),

                                                                 ((-18,-6, 14,  8, 66),
                                                                  ( 4, 26, 42,150,172),
                                                                  (22, 58,104,184,222),
                                                                  (90,148,186,350,400))),

                                                                ((( 0,  0, 20,  0,140),
                                                                  ( 0, 20, 60,220,300),
                                                                  (20, 60,160,300,340),
                                                                 (140,220,300,400,400)),

                                                                 ((-12, 0, 28, 16,160),
                                                                  ( 4, 36, 64,232,316),
                                                                  (24, 72,176,312,364),
                                                                 (148,244,328,480,500)),

                                                                 ((-20,-4, 44, 24,168),
                                                                  (12, 48, 76,260,336),
                                                                  (36,100,200,344,380),
                                                                 (164,268,356,520,600)),

                                                                 ((-36,-12,28, 16,132),
                                                                  ( 8, 52, 84,300,344),
                                                                  (44,116,208,368,444),
                                                                 (180,296,372,700,800))),

                                                                ((( 0,  0,  5,  0, 35),
                                                                  ( 0,  5, 15, 55, 75),
                                                                  ( 5, 15, 40, 75, 85),
                                                                  (35, 55, 75,100,100)),

                                                                 ((-3,  0,  7,  4, 40),
                                                                  ( 1,  9, 16, 58, 79),
                                                                  ( 6, 18, 44, 78, 91),
                                                                  (37, 61, 82,120,125)),

                                                                 ((-5, -1, 11,  6, 42),
                                                                  ( 3, 12, 19, 65, 84),
                                                                  ( 9, 25, 50, 86, 95),
                                                                  (41, 67, 89,130,150)),

                                                                 ((-9, -3,  7,  4, 33),
                                                                  ( 2, 13, 21, 75, 86),
                                                                  (11, 29, 52, 92,111),
                                                                  (45, 74, 93,175,200))));
var
  QueryResult: TInterpolatedType;
begin
  QueryResult := Interpolate4D(18, -17, 25, -50,
                               @Table4D,
                               TABLE4D_DIM_X, TABLE4D_START_X, TABLE4D_STEP_X,
                               TABLE4D_DIM_Y, TABLE4D_START_Y, TABLE4D_STEP_Y,
                               TABLE4D_DIM_Z, TABLE4D_START_Z, TABLE4D_STEP_Z,
                               TABLE4D_DIM_U, TABLE4D_START_U, TABLE4D_STEP_U);
  Memo1.Append('Interpolated 4D:   F(18,-17,25,-50)=' + QueryResult.ToString);
end; 
```

```
  Interpolated 4D: F(18,-17,25,-50)=14.2
```



### Using dynamic arrays

- **1D** - One dimensional linear interpolation example using dynamic array:

```pascal
uses
  DynamicInterpolation;
...
procedure TForm1.Calc1DClick(Sender: TObject);
const
  TABLE1D_START_X =   0; // first element of X table
  TABLE1D_STEP_X  =  10; // increase step for each following element
                         // this means that X table is this:      ( 0, 10, 20, 30, 40)
var
  DynTable1D: TArr1D;
  QueryResult: TInterpolatedType;
begin
  DynTable1D  := TArr1D.Create( 0,  0,  5,  0, 35); // F(X)=DynTable1D lookup table
  QueryResult := Interpolate1D(15,
                               DynTable1D,
                               TABLE1D_START_X, TABLE1D_STEP_X);
  Memo1.Append('Interpolated 1D: F(15)=' + QueryResult.ToString);
end;
```

```
  Interpolated 1D: F(15)=2.5
```

- **2D** - Two dimensional linear interpolation example using dynamic arrays:

```pascal
procedure TForm1.Calc2DClick(Sender: TObject);
const
  TABLE2D_START_X =   0;
  TABLE2D_STEP_X  =  10;
  TABLE2D_START_Y = -20;
  TABLE2D_STEP_Y  =  10;    
var
  DynTable2D: TArr2D;
  QueryResult: TInterpolatedType;
begin
  DynTable2D  := TArr2D.Create(TArr1D.Create( 0,  0,  5,  0, 35),
                               TArr1D.Create( 0,  5, 15, 55, 75),
                               TArr1D.Create( 5, 15, 40, 75, 85),
                               TArr1D.Create(35, 55, 75,100,100));
  QueryResult := Interpolate2D(18, -17,
                               DynTable2D,
                               TABLE2D_START_X, TABLE2D_STEP_X,
                               TABLE2D_START_Y, TABLE2D_STEP_Y);
  Memo1.Append('Interpolated 2D: F(18,-17)=' + QueryResult.ToString);
end; 
```

```
  Interpolated 2D: F(18,-17)=6.7
```

- **3D** - Three dimensional linear interpolation example using dynamic arrays:

```pascal
procedure TForm1.Calc3DClick(Sender: TObject);
const
  TABLE3D_START_X =   0;
  TABLE3D_STEP_X  =  10;
  TABLE3D_START_Y = -20;
  TABLE3D_STEP_Y  =  10;
  TABLE3D_START_Z =   0;
  TABLE3D_STEP_Z  = 100;   
var
  DynTable3D: TArr3D;
  QueryResult: TInterpolatedType;
begin
  DynTable3D  := TArr3D.Create(TArr2D.Create(TArr1D.Create( 0,  0,  5,  0, 35),
                                             TArr1D.Create( 0,  5, 15, 55, 75),
                                             TArr1D.Create( 5, 15, 40, 75, 85),
                                             TArr1D.Create(35, 55, 75,100,100)),

                               TArr2D.Create(TArr1D.Create(-3,  0,  7,  4, 40),
                                             TArr1D.Create( 1,  9, 16, 58, 79),
                                             TArr1D.Create( 6, 18, 44, 78, 91),
                                             TArr1D.Create(37, 61, 82,120,125)),

                               TArr2D.Create(TArr1D.Create(-5, -1, 11,  6, 42),
                                             TArr1D.Create( 3, 12, 19, 65, 84),
                                             TArr1D.Create( 9, 25, 50, 86, 95),
                                             TArr1D.Create(41, 67, 89,130,150)),

                               TArr2D.Create(TArr1D.Create(-9, -3,  7,  4, 33),
                                             TArr1D.Create( 2, 13, 21, 75, 86),
                                             TArr1D.Create(11, 29, 52, 92,111),
                                             TArr1D.Create(45, 74, 93,175,200)));
QueryResult := Interpolate3D(18, -17, 25,
                               DynTable3D,
                               TABLE3D_START_X, TABLE3D_STEP_X,
                               TABLE3D_START_Y, TABLE3D_STEP_Y,
                               TABLE3D_START_Z, TABLE3D_STEP_Z);
  Memo1.Append('Interpolated 3D: F(18,-17,25)=' + QueryResult.ToString);
end;
```

```
  Interpolated 3D: F(18,-17,25)=7.1
```

- **4D** - Four dimensional linear interpolation example using dynamic arrays:

```pascal
procedure TForm1.Calc4DClick(Sender: TObject);
const
  TABLE4D_START_X =   0;
  TABLE4D_STEP_X  =  10;
  TABLE4D_START_Y = -20;
  TABLE4D_STEP_Y  =  10;
  TABLE4D_START_Z =   0;
  TABLE4D_STEP_Z  = 100;
  TABLE4D_START_U =-150;
  TABLE4D_STEP_U  = 100; 
var
  DynTable4D: TArr4D;
  QueryResult: TInterpolatedType;
begin
  DynTable4D := TArr4D.Create(
                TArr3D.Create(TArr2D.Create(TArr1D.Create( 0,  0,  5,  0, 35),
                                            TArr1D.Create( 0,  5, 15, 55, 75),
                                            TArr1D.Create( 5, 15, 40, 75, 85),
                                            TArr1D.Create(35, 55, 75,100,100)),
                              TArr2D.Create(TArr1D.Create(-3,  0,  7,  4, 40),
                                            TArr1D.Create( 1,  9, 16, 58, 79),
                                            TArr1D.Create( 6, 18, 44, 78, 91),
                                            TArr1D.Create(37, 61, 82,120,125)),
                              TArr2D.Create(TArr1D.Create(-5, -1, 11,  6, 42),
                                            TArr1D.Create( 3, 12, 19, 65, 84),
                                            TArr1D.Create( 9, 25, 50, 86, 95),
                                            TArr1D.Create(41, 67, 89,130,150)),
                              TArr2D.Create(TArr1D.Create(-9, -3,  7,  4, 33),
                                            TArr1D.Create( 2, 13, 21, 75, 86),
                                            TArr1D.Create(11, 29, 52, 92,111),
                                            TArr1D.Create(45, 74, 93,175,200))),
                TArr3D.Create(TArr2D.Create(TArr1D.Create( 0,  0, 10,  0, 70),
                                            TArr1D.Create( 0, 10, 30,110,150),
                                            TArr1D.Create(10, 30, 80,150,170),
                                            TArr1D.Create(70,110,150,200,200)),
                              TArr2D.Create(TArr1D.Create(-6,  0, 14,  8, 80),
                                            TArr1D.Create( 2, 18, 32,116,158),
                                            TArr1D.Create(12, 36, 88,156,182),
                                            TArr1D.Create(74,122,164,240,250)),
                              TArr2D.Create(TArr1D.Create(-10,-2, 22, 12, 84),
                                            TArr1D.Create( 6, 24, 38,130,168),
                                            TArr1D.Create(18, 50,100,172,190),
                                            TArr1D.Create(82,134,178,260,300)),
                              TArr2D.Create(TArr1D.Create(-18,-6, 14,  8, 66),
                                            TArr1D.Create( 4, 26, 42,150,172),
                                            TArr1D.Create(22, 58,104,184,222),
                                            TArr1D.Create(90,148,186,350,400))),
                TArr3D.Create(TArr2D.Create(TArr1D.Create( 0,  0, 20,  0,140),
                                            TArr1D.Create( 0, 20, 60,220,300),
                                            TArr1D.Create(20, 60,160,300,340),
                                            TArr1D.Create(140,220,300,400,400)),
                              TArr2D.Create(TArr1D.Create(-12, 0, 28, 16,160),
                                            TArr1D.Create( 4, 36, 64,232,316),
                                            TArr1D.Create(24, 72,176,312,364),
                                            TArr1D.Create(148,244,328,480,500)),
                              TArr2D.Create(TArr1D.Create(-20,-4, 44, 24,168),
                                            TArr1D.Create(12, 48, 76,260,336),
                                            TArr1D.Create(36,100,200,344,380),
                                            TArr1D.Create(164,268,356,520,600)),
                              TArr2D.Create(TArr1D.Create(-36,-12,28, 16,132),
                                            TArr1D.Create( 8, 52, 84,300,344),
                                            TArr1D.Create(44,116,208,368,444),
                                            TArr1D.Create(180,296,372,700,800))),
                TArr3D.Create(TArr2D.Create(TArr1D.Create( 0,  0,  5,  0, 35),
                                            TArr1D.Create( 0,  5, 15, 55, 75),
                                            TArr1D.Create( 5, 15, 40, 75, 85),
                                            TArr1D.Create(35, 55, 75,100,100)),
                              TArr2D.Create(TArr1D.Create(-3,  0,  7,  4, 40),
                                            TArr1D.Create( 1,  9, 16, 58, 79),
                                            TArr1D.Create( 6, 18, 44, 78, 91),
                                            TArr1D.Create(37, 61, 82,120,125)),
                              TArr2D.Create(TArr1D.Create(-5, -1, 11,  6, 42),
                                            TArr1D.Create( 3, 12, 19, 65, 84),
                                            TArr1D.Create( 9, 25, 50, 86, 95),
                                            TArr1D.Create(41, 67, 89,130,150)),
                              TArr2D.Create(TArr1D.Create(-9, -3,  7,  4, 33),
                                            TArr1D.Create( 2, 13, 21, 75, 86),
                                            TArr1D.Create(11, 29, 52, 92,111),
                                            TArr1D.Create(45, 74, 93,175,200))));
  QueryResult := Interpolate4D(18, -17, 25, -50,
                               DynTable4D,
                               TABLE4D_START_X, TABLE4D_STEP_X,
                               TABLE4D_START_Y, TABLE4D_STEP_Y,
                               TABLE4D_START_Z, TABLE4D_STEP_Z,
                               TABLE4D_START_U, TABLE4D_STEP_U);
  Memo1.Append('Interpolated 4D:   F(18,-17,25,-50)=' + QueryResult.ToString);
end; 
```

```
  Interpolated 4D: F(18,-17,25,-50)=14.2
```



### Download ###

If for some reason you do not handle git, then full repository can be downloaded manually from [here](https://bitbucket.org/avra/interpolate1234d/downloads).



### License ###

* **Interpolate1234D** is released under commercial friendly [Mozilla Public License 2.0 (MPL-2.0)](https://www.mozilla.org/MPL/2.0)
* [License explained in plain English](https://www.tldrlegal.com/l/mpl-2.0)



### Author ###

Made by Zeljko Avramovic (user Avra in Lazarus forum). Big thanks go to Gernot Hoffmann for his paper [Multidimensional Linear Interpolation](https://bitbucket.org/avra/interpolate1234d/downloads/Multidimensional%20Linear%20Interpolation%20in%20Pascal.pdf).



### Versions ###

* **1.4**   Split library into DynamicInterpolation and StaticInterpolation. That way library is much easier to use in embedded devices with limited resources.
* **1.2**   Added support for dynamic arrays. Added 3D printer bed level mesh map visualization demo.
* **1.0**   First public version with support for static arrays.