unit staticinterpolation;

{$mode objfpc}{$H+}

{$WARN 4080 off : Converting the operands to "$1" before doing the subtract could prevent overflow errors.}

interface

uses
  Classes, SysUtils, Math;

type
  TInterpolatedType = double;
  PInterpolatedType = ^TInterpolatedType;

  function Interpolate1D(const x: TInterpolatedType;
                         const ATable1D: PInterpolatedType; // static array version
                         const ATableDimensionX: word; const ATableStartX, ATableStepX: TInterpolatedType): TInterpolatedType;

  function Interpolate2D(const x, y: TInterpolatedType;
                         const ATable2D: PInterpolatedType; // static array version
                         const ATableDimensionX: word; const ATableStartX, ATableStepX: TInterpolatedType;
                         const ATableDimensionY: word; const ATableStartY, ATableStepY: TInterpolatedType): TInterpolatedType;

  function Interpolate3D(const x, y, z: TInterpolatedType;
                         const ATable3D: PInterpolatedType; // static array version
                         const ATableDimensionX: word; const ATableStartX, ATableStepX: TInterpolatedType;
                         const ATableDimensionY: word; const ATableStartY, ATableStepY: TInterpolatedType;
                         const ATableDimensionZ: word; const ATableStartZ, ATableStepZ: TInterpolatedType): TInterpolatedType;

  function Interpolate4D(const x, y, z, u: TInterpolatedType;
                         const ATable4D: PInterpolatedType; // static array version
                         const ATableDimensionX: word; const ATableStartX, ATableStepX: TInterpolatedType;
                         const ATableDimensionY: word; const ATableStartY, ATableStepY: TInterpolatedType;
                         const ATableDimensionZ: word; const ATableStartZ, ATableStepZ: TInterpolatedType;
                         const ATableDimensionU: word; const ATableStartU, ATableStepU: TInterpolatedType): TInterpolatedType;

implementation


// n = 1
// Fx = px F1 + qx F2
function Interpolate1D(const x: TInterpolatedType;
                       const ATable1D: PInterpolatedType; // static array version
                       const ATableDimensionX: word; const ATableStartX, ATableStepX: TInterpolatedType): TInterpolatedType;
var // O(1) time complexity achieved
  ix: word;
  x1: TInterpolatedType;
  px, qx: TInterpolatedType;
  f1, f2: TInterpolatedType;
  pf: PInterpolatedType; // for optimized pointer arithmetic
begin
  // calculate column of F1
  ix := Min(Trunc((x - ATableStartX) / ATableStepX) + 1, ATableDimensionX - 1); // Min() ensures that x1 is left of x2 if x2 is last table element

  pf := ATable1D + ix - 1; // pointer to F1 (first table element left of x)

  // Table1D x axis values are not stored. They can all be calculated from ATableStartX and ATableStepX parameters
  // but no need for finding them all. We can just find x1 which is left of x and x2 which is right of x.
  x1 := ATableStartX + (ix - 1) * ATableStepX;

  // get F1 and F2 array items with x in between them
  f1 := (pf)^;
  f2 := (pf + 1)^;

  // calculate relative distances of x from x1 and x2
  qx := (x - x1) / ATableStepX;
  px := 1.0 - qx;

  // calculate Fx
  Result := px * f1 + qx * f2;
end;

{ old not optimized version:
function Interpolate1D(const x: TInterpolatedType;
                       const ATable1D: PInterpolatedType; // static array version
                       const ATableDimensionX: word; const ATableStartX, ATableStepX: TInterpolatedType): TInterpolatedType;
var // O(1) time complexity achieved
  ix: word;
  x1, x2, f1, f2, px, qx: TInterpolatedType;
begin
  // calculate column of F1
  ix := Min(Trunc((x - ATableStart) / ATableStep) + 1, ATableDimension - 1); // Min() ensures that x1 is left of x2 if x2 is last table element

  // Table1D x axis values are not stored. They can all be calculated from ATableStart and ATableStep parameters
  // but no need for finding them all. We can just find x1 which is left of x and x2 which is right of x.
  x1 := ATableStart + (ix - 1) * ATableStep;
  x2 := x1 + ATableStep;

  // get F1 and F2 array items with x in between them
  f1 := (ATable1D + ix - 1)^;
  f2 := (ATable1D + ix)^;

  // calculate relative distances of x from x1 and x2
  qx := (x - x1) / (x2 - x1);
  px := 1.0 - qx;

  // calculate Fx
  Result := px * f1 + qx * f2;
end;}



{ n = 2   2D interpolation example:

          Window of 4 neighouring points F11, F21, F12 and F22 from a much bigger Table2D array:

          x1          x              x2
          .           .               .
          .           .               .
          .<-- qx% -->.<-----px%----->.
 y1 . . .F11----------+--------------F21
        ^ |           |               |     F11 = Table2D[ix,iy]
        | |           |               |
       qy%|           |               |     ix is column of F11 in 2D array
        | |           |               |     iy is row    of F11 in 2D array
        v |           |               |
y . . . . +----------Fxy--------------+     All we need to calculate interpolated value F(x,y) is to
        ^ |           |               |     find values of 4 neighbouring points F11, F21, F12 and F22
        | |           |               |     having x and y between them, calculate relative distances
       py%|           |               |     q1 and q2 from F11 to Fxy, and use simple 1D linear
        | |           |               |     interpolation in both directions.
 y2 . . .F12----------+--------------F22

          qx  = (x - x1) / (x2 - x1)        0.00 .. 1.00 (out of this range only in case of extrapolation)
          qy  = (y - y1) / (y2 - y1)        0.00 .. 1.00 (out of this range only in case of extrapolation)
          px  = 1.0 - qx                    0.00 .. 1.00 (out of this range only in case of extrapolation)
          py  = 1.0 - qy                    0.00 .. 1.00 (out of this range only in case of extrapolation)
          Fx1 = px F11 + qx F21
          Fx2 = px F12 + qx F22
          Fxy = py Fx1 + qy Fx2
}
function Interpolate2D(const x, y: TInterpolatedType;
                       const ATable2D: PInterpolatedType; // static array version
                       const ATableDimensionX: word; const ATableStartX, ATableStepX: TInterpolatedType;
                       const ATableDimensionY: word; const ATableStartY, ATableStepY: TInterpolatedType): TInterpolatedType;
var
  ix, iy: word;
  x1, y1: TInterpolatedType;
  px, qx, py, qy: TInterpolatedType;
  f11, f21, f12, f22, fx1, fx2: TInterpolatedType;
  pf: PInterpolatedType; // for optimized pointer arithmetic
  {IncX,} IncY: integer;
begin
  // IncX := 1;
  IncY := ATableDimensionX;

  ix  := Min(Trunc((x - ATableStartX) / ATableStepX) + 1, ATableDimensionX - 1); // calculate column of F11
  iy  := Min(Trunc((y - ATableStartY) / ATableStepY) + 1, ATableDimensionY - 1); // calculate row    of F11

  pf  := ATable2D + (iy - 1) * ATableDimensionX + ix - 1; // pointer to F11 (first table element upper left from x)            ATable2D + (iy - 1) * ATableDimensionX + (ix - 1) * IncX

  x1  := ATableStartX + (ix - 1) * ATableStepX;
  qx  := (x - x1) / ATableStepX;
  px  := 1.0 - qx;
  f11 := (pf)^;                                           // avoid micro optimization (one decrement could be avoided if pf pointed to f21 instead of f11, but readability would suffer)
  f21 := (pf + 1)^;
  fx1 := px * f11 + qx * f21;

  y1  := ATableStartY + (iy - 1) * ATableStepY;
  qy  := (y - y1) / ATableStepY;
  py  := 1.0 - qy;
  // old:
  // pf := ATable2D + iy * ATableDimensionX + ix - 1;
  // f12 := (pf)^;
  // f22 := (pf + 1)^;
  // new:
  f12 := (pf + IncY)^;
  f22 := (pf + IncY + 1)^;
  fx2 := px * f12 + qx * f22;

  Result := py * fx1 + qy * fx2; // Fxy = Result
end;

{ old not optimized version:
function Interpolate2D(const x, y: TInterpolatedType;
                       const ATable2D: PInterpolatedType; // static array version
                       const ATableDimensionX: word; const ATableStartX, ATableStepX: TInterpolatedType;
                       const ATableDimensionY: word; const ATableStartY, ATableStepY: TInterpolatedType): TInterpolatedType;
var
  ix, iy: word;
  x1, x2, f11, f21, px, qx, fx1: TInterpolatedType;
  y1, y2, f12, f22, py, qy, fx2: TInterpolatedType;
  pf: PInterpolatedType; // for optimized pointer arithmetic
begin
  ix  := Min(Trunc((x - ATableStartX) / ATableStepX) + 1, ATableDimensionX - 1); // calculate column of F11
  iy  := Min(Trunc((y - ATableStartY) / ATableStepY) + 1, ATableDimensionY - 1); // calculate row    of F11

  x1  := ATableStartX + (ix - 1) * ATableStepX;
  x2  := x1 + ATableStepX;
  qx  := (x - x1) / (x2 - x1);
  // qx  := (x - x1) / ATableStepX; // potential future optimization because (x2 - x1) = (x2 - x1 + x1 + ATableStepX)
  px  := 1.0 - qx;
  // f11 := (ATable2D + (iy - 1) * ATableDimensionX + ix - 1)^; // no optimization
  // f21 := (ATable2D + (iy - 1) * ATableDimensionX + ix)^;     // no optimization
  pf  := ATable2D + (iy - 1) * ATableDimensionX + ix - 1;       // optimization to avoid duplicated pointer calculation
  f11 := (pf)^;                                                 // avoided further micro optimization (one decrement could be avoided if pf pointed to f21 instead of f11, but readability would suffer)
  f21 := (pf + 1)^;
  fx1 := px * f11 + qx * f21;

  y1  := ATableStartY + (iy - 1) * ATableStepY;
  y2  := y1 + ATableStepY;
  qy  := (y - y1) / (y2 - y1);
  py  := 1.0 - qy;
  // f12 := (ATable2D + iy * ATableDimensionX + ix - 1)^;
  // f22 := (ATable2D + iy * ATableDimensionX + ix)^;
  pf  := ATable2D + iy * ATableDimensionX + ix - 1;
  f12 := (pf)^;
  f22 := (pf + 1)^;
  fx2 := px * f12 + qx * f22;

  Result := py * fx1 + qy * fx2; // Fxy = Result
end;}




// n = 3
// Fx11 = px F111 + qx F211
// Fx21 = px F121 + qx F221
// Fxy1 = py Fx11 + qy Fx21
// Fx12 = px F112 + qx F212
// Fx22 = px F122 + qx F222
// Fxy2 = py Fx12 + qy Fx22
// Fxyz = pz Fxy1 + qz Fxy2
function Interpolate3D(const x, y, z: TInterpolatedType;
                       const ATable3D: PInterpolatedType; // static array version
                       const ATableDimensionX: word; const ATableStartX, ATableStepX: TInterpolatedType;
                       const ATableDimensionY: word; const ATableStartY, ATableStepY: TInterpolatedType;
                       const ATableDimensionZ: word; const ATableStartZ, ATableStepZ: TInterpolatedType): TInterpolatedType;
var
  ix, iy, iz: word;
  x1, y1, z1: TInterpolatedType;
  px, qx, py, qy, pz, qz: TInterpolatedType;
  f111, f211, f121, f221, f112, f212, f122, f222, fx11, fx21, fx12, fx22, fxy1, fxy2: TInterpolatedType;
  pf: PInterpolatedType; // for optimized pointer arithmetic
  {IncX,} IncY, IncZ: integer;
begin
  // IncX := 1;
  IncY := ATableDimensionX;
  IncZ := ATableDimensionX * ATableDimensionY;

  ix   := Min(Trunc((x - ATableStartX) / ATableStepX) + 1, ATableDimensionX - 1); // calculate column of F111
  iy   := Min(Trunc((y - ATableStartY) / ATableStepY) + 1, ATableDimensionY - 1); // calculate row    of F111
  iz   := Min(Trunc((z - ATableStartZ) / ATableStepZ) + 1, ATableDimensionZ - 1); // calculate depth  of F111

  pf   := ATable3D + (iz - 1) * IncZ + (iy - 1) * IncY + ix - 1; // pointer to F111     ATable3D + (iz - 1) * IncZ + (iy - 1) * IncY + (ix - 1) * IncX

  x1   := ATableStartX + (ix - 1) * ATableStepX;
  qx   := (x - x1) / ATableStepX;
  px   := 1.0 - qx;
  f111 := (pf)^;
  f211 := (pf + 1)^;
  fx11 := px * f111 + qx * f211;

  y1   := ATableStartY + (iy - 1) * ATableStepY;
  qy   := (y - y1) / ATableStepY;
  py   := 1.0 - qy;
  // old:
  // pf   := ATable3D + (iz - 1) * IncZ + iy * IncY + ix - 1;
  // f121 := (pf)^;
  // f221 := (pf + 1)^;
  // new:
  f121 := (pf + IncY)^;
  f221 := (pf + IncY + 1)^;
  fx21 := px * f121 + qx * f221;

  fxy1 := py * fx11 + qy * fx21;

  z1   := ATableStartZ + (iz - 1) * ATableStepZ;
  qz   := (z - z1) / ATableStepZ;
  pz   := 1.0 - qz;
  // old:
  // pf   := ATable3D + iz * IncZ + (iy - 1) * IncY + ix - 1;
  // f112 := (pf)^;
  // f212 := (pf + 1)^;
  // new:
  f112 := (pf + IncZ)^;
  f212 := (pf + IncZ + 1)^;
  fx12 := px * f112 + qx * f212;
  // old:
  // pf   := ATable3D + iz * IncZ + iy * IncY + ix - 1;
  // f122 := (pf)^;
  // f222 := (pf + 1)^;
  // new:
  f122 := (pf + IncZ + IncY)^;
  f222 := (pf + IncZ + IncY + 1)^;
  fx22 := px * f122 + qx * f222;

  fxy2 := py * fx12 + qy * fx22;

  Result := pz * fxy1 + qz * fxy2; // Fxyz = Result
end;

{ old not optimized version:
function Interpolate3D(const x, y, z: TInterpolatedType;
                       const ATable3D: PInterpolatedType;
                       const ATableDimensionX: word; const ATableStartX, ATableStepX: TInterpolatedType;
                       const ATableDimensionY: word; const ATableStartY, ATableStepY: TInterpolatedType;
                       const ATableDimensionZ: word; const ATableStartZ, ATableStepZ: TInterpolatedType): TInterpolatedType;
var
  ix, iy, iz: word;
  x1, x2, f111, f211,             px, qx, fx11, fx21: TInterpolatedType;
  y1, y2, f121, f221,             py, qy, fx12, fx22: TInterpolatedType;
  z1, z2, f112, f212, f122, f222, pz, qz, fxy1, fxy2: TInterpolatedType;
  pf: PInterpolatedType; // for optimized pointer arithmetic
begin
  ix   := Min(Trunc((x - ATableStartX) / ATableStepX) + 1, ATableDimensionX - 1); // calculate column of F111
  iy   := Min(Trunc((y - ATableStartY) / ATableStepY) + 1, ATableDimensionY - 1); // calculate row    of F111
  iz   := Min(Trunc((z - ATableStartZ) / ATableStepZ) + 1, ATableDimensionZ - 1); // calculate depth  of F111

  x1   := ATableStartX + (ix - 1) * ATableStepX;
  x2   := x1 + ATableStepX;
  qx   := (x - x1) / (x2 - x1);
  px   := 1.0 - qx;
  pf   := ATable3D + (iz - 1) * ATableDimensionX * ATableDimensionY + (iy - 1) * ATableDimensionX + ix - 1;
  f111 := (pf)^;
  f211 := (pf + 1)^;
  fx11 := px * f111 + qx * f211;

  y1   := ATableStartY + (iy - 1) * ATableStepY;
  y2   := y1 + ATableStepY;
  qy   := (y - y1) / (y2 - y1);
  py   := 1.0 - qy;
  pf   := ATable3D + (iz - 1) * ATableDimensionX * ATableDimensionY + iy * ATableDimensionX + ix - 1;
  f121 := (pf)^;
  f221 := (pf + 1)^;
  fx21 := px * f121 + qx * f221;

  fxy1 := py * fx11 + qy * fx21;

  z1  := ATableStartZ + (iz - 1) * ATableStepZ;
  z2  := z1 + ATableStepZ;
  qz   := (z - z1) / (z2 - z1);
  pz   := 1.0 - qz;
  pf   := ATable3D + iz * ATableDimensionX * ATableDimensionY + (iy - 1) * ATableDimensionX + ix - 1;
  f112 := (pf)^;
  f212 := (pf + 1)^;
  fx12 := px * f112 + qx * f212;
  pf   := ATable3D + iz * ATableDimensionX * ATableDimensionY + iy * ATableDimensionX + ix - 1;
  f122 := (pf)^;
  f222 := (pf + 1)^;
  fx22 := px * f122 + qx * f222;

  fxy2 := py * fx12 + qy * fx22;

  Result := pz * fxy1 + qz * fxy2; // Fxyz = Result
end;}





// n = 4
// Fx111 = px F1111 + qx F2111
// Fx211 = px F1211 + qx F2211
// Fx121 = px F1121 + qx F2121
// Fx221 = px F1221 + qx F2221
// Fxy11 = py Fx111 + qy Fx211
// Fxy21 = py Fx121 + qy Fx221
// Fxyz1 = pz Fxy11 + qz Fxy21
// Fx112 = px F1112 + qx F2112
// Fx212 = px F1212 + qx F2212
// Fx122 = px F1122 + qx F2122
// Fx222 = px F1222 + qx F2222
// Fxy12 = py Fx112 + qy Fx212
// Fxy22 = py Fx122 + qy Fx222
// Fxyz2 = pz Fxy12 + qz Fxy22
// Fxyzu = pu Fxyz1 + qu Fxyz2
function Interpolate4D(const x, y, z, u: TInterpolatedType;
                       const ATable4D: PInterpolatedType; // static array version
                       const ATableDimensionX: word; const ATableStartX, ATableStepX: TInterpolatedType;
                       const ATableDimensionY: word; const ATableStartY, ATableStepY: TInterpolatedType;
                       const ATableDimensionZ: word; const ATableStartZ, ATableStepZ: TInterpolatedType;
                       const ATableDimensionU: word; const ATableStartU, ATableStepU: TInterpolatedType): TInterpolatedType;
var
  ix, iy, iz, iu: word;
  x1, x2, f1111, f2111,                                           px, qx, fx111, fx211: TInterpolatedType;
  y1, y2, f1211, f2211,                                           py, qy, fx121, fx221: TInterpolatedType;
  z1, z2, f1121, f2121, f1221, f2221,                             pz, qz, fxy11, fxy21: TInterpolatedType;
  u1, u2, f1112, f2112, f1212, f2212, f1122, f2122, f1222, f2222, pu, qu, fx112, fx212, fx122, fx222, fxy12, fxy22, fxyz1, fxyz2: TInterpolatedType;
  pf: PInterpolatedType; // for optimized pointer arithmetic
  {IncX,} IncY, IncZ, IncU: integer;
begin
  //IncX := 1;
  IncY  := ATableDimensionX;                                                       // more clear code
  IncZ  := ATableDimensionX * ATableDimensionY;                                    // minor optimization
  IncU  := ATableDimensionX * ATableDimensionY * ATableDimensionZ;                 // minor optimization

  ix    := Min(Trunc((x - ATableStartX) / ATableStepX) + 1, ATableDimensionX - 1); // calculate column of F1111
  iy    := Min(Trunc((y - ATableStartY) / ATableStepY) + 1, ATableDimensionY - 1); // calculate row    of F1111
  iz    := Min(Trunc((z - ATableStartZ) / ATableStepZ) + 1, ATableDimensionZ - 1); // calculate depth  of F1111
  iu    := Min(Trunc((u - ATableStartU) / ATableStepU) + 1, ATableDimensionU - 1); // calculate volume of F1111

  pf    := ATable4D + (iu - 1) * IncU + (iz - 1) * IncZ + (iy - 1) * IncY + ix - 1; // Pointer to F1111     ATable4D + (iu - 1) * IncU + (iz - 1) * IncZ + (iy - 1) * IncY + (ix - 1) * IncX

  // X axis:
  x1    := ATableStartX + (ix - 1) * ATableStepX;
  x2    := x1 + ATableStepX;
  qx    := (x - x1) / (x2 - x1);
  px    := 1.0 - qx;
  f1111 := (pf)^;
  f2111 := (pf + 1)^;
  fx111 := px * f1111 + qx * f2111;

  // Y axis:
  y1    := ATableStartY + (iy - 1) * ATableStepY;
  y2    := y1 + ATableStepY;
  qy    := (y - y1) / (y2 - y1);
  py    := 1.0 - qy;
  // old:
  // pf    := ATable4D + (iu - 1) * IncU + (iz - 1) * IncZ + iy * IncY + ix - 1;
  // f1211 := (pf)^;
  // f2211 := (pf + 1)^;
  // new:
  f1211 := (pf + IncY)^;
  f2211 := (pf + IncY + 1)^;
  fx211 := px * f1211 + qx * f2211;

  fxy11 := py * fx111 + qy * fx211;

  // Z axis:
  z1    := ATableStartZ + (iz - 1) * ATableStepZ;
  z2    := z1 + ATableStepZ;
  qz    := (z - z1) / (z2 - z1);
  pz    := 1.0 - qz;
  // old:
  // pf    := ATable4D + (iu - 1) * IncU + iz * IncZ + (iy - 1) * IncY + ix - 1;
  // f1121 := (pf)^;
  // f2121 := (pf + 1)^;
  // new:
  f1121 := (pf + IncZ)^;
  f2121 := (pf + IncZ + 1)^;
  fx121 := px * f1121 + qx * f2121;
  // old:
  // pf    := ATable4D + (iu - 1) * IncU + iz * IncZ + iy * IncY + ix - 1;
  // f1221 := (pf)^;
  // f2221 := (pf + 1)^;
  // new:
  f1221 := (pf + IncZ + IncY)^;
  f2221 := (pf + IncZ + IncY + 1)^;
  fx221 := px * f1221 + qx * f2221;

  fxy21 := py * fx121 + qy * fx221;

  fxyz1 := pz * fxy11 + qz * fxy21;

  // U axis:
  u1    := ATableStartU + (iu - 1) * ATableStepU;
  u2    := u1 + ATableStepU;
  qu    := (u - u1) / (u2 - u1);
  pu    := 1.0 - qu;
  // old:
  // pf    := ATable4D + iu * IncU + (iz - 1) * IncZ + (iy - 1) * IncY + ix - 1;
  // f1112 := (pf)^;
  // f2112 := (pf + 1)^;
  // new:
  f1112 := (pf + IncU)^;
  f2112 := (pf + IncU + 1)^;
  fx112 := px * f1112 + qx * f2112;
  // old:
  // pf    := ATable4D + iu * IncU + (iz - 1) * IncZ + iy * IncY + ix - 1;
  // f1212 := (pf)^;
  // f2212 := (pf + 1)^;
  // new:
  f1212 := (pf + IncU + IncY)^;
  f2212 := (pf + IncU + IncY + 1)^;
  fx212 := px * f1212 + qx * f2212;

  fxy12 := py * fx112+ qy * fx212;

  // old:
  // pf    := ATable4D + iu * IncU + iz * IncZ + (iy - 1) * IncY + ix - 1;
  // f1122 := (pf)^;
  // f2122 := (pf + 1)^;
  // new:
  f1122 := (pf + IncU + IncZ)^;
  f2122 := (pf + IncU + IncZ + 1)^;
  fx122 := px * f1122 + qx * f2122;
  // old:
  // pf    := ATable4D + iu * IncU + iz * IncZ + iy * IncY + ix - 1;
  // f1222 := (pf)^;
  // f2222 := (pf + 1)^;
  // new:
  f1222 := (pf + IncU + IncZ + IncY)^;
  f2222 := (pf + IncU + IncZ + IncY + 1)^;
  fx222 := px * f1222 + qx * f2222;

  fxy22 := py * fx122+ qy * fx222;

  fxyz2 := pz * fxy12 + qz * fxy22;

  Result := pu * fxyz1 + qu * fxyz2; // Fxyzu = Result
end;

end.

