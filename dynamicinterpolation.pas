unit dynamicinterpolation;

{$mode objfpc}{$H+}

{$WARN 4080 off : Converting the operands to "$1" before doing the subtract could prevent overflow errors.}

interface

uses
  Classes, SysUtils, Math;

type
  TInterpolatedType = double;
  PInterpolatedType = ^TInterpolatedType;

  TArr1D = array of TInterpolatedType;
  TArr2D = array of TArr1D;
  TArr3D = array of TArr2D;
  TArr4D = array of TArr3D;

  function Interpolate1D(const x: TInterpolatedType;
                         const ATable1D: TArr1D;            // dynamic array version
                         const ATableStart, ATableStep: TInterpolatedType): TInterpolatedType;

  function Interpolate2D(const x, y: TInterpolatedType;
                         const ATable2D: TArr2D;            // dynamic array version
                         const ATableStartX, ATableStepX: TInterpolatedType;
                         const ATableStartY, ATableStepY: TInterpolatedType): TInterpolatedType;

  function Interpolate3D(const x, y, z: TInterpolatedType;
                         const ATable3D: TArr3d;           // dynamic array version
                         const ATableStartX, ATableStepX: TInterpolatedType;
                         const ATableStartY, ATableStepY: TInterpolatedType;
                         const ATableStartZ, ATableStepZ: TInterpolatedType): TInterpolatedType;

  function Interpolate4D(const x, y, z, u: TInterpolatedType;
                         const ATable4D: TArr4D; // dynamic array version
                         const ATableStartX, ATableStepX: TInterpolatedType;
                         const ATableStartY, ATableStepY: TInterpolatedType;
                         const ATableStartZ, ATableStepZ: TInterpolatedType;
                         const ATableStartU, ATableStepU: TInterpolatedType): TInterpolatedType;

implementation


function Interpolate1D(const x: TInterpolatedType;
                       const ATable1D: TArr1D; // new dynamic array version
                       const ATableStart, ATableStep: TInterpolatedType): TInterpolatedType;
var // O(1) time complexity achieved
  ix: word;
  x1: TInterpolatedType;
  px, qx: TInterpolatedType;
  f1, f2: TInterpolatedType;
  TableDimensionX: word;
begin
  TableDimensionX := Length(ATable1D);

  // calculate column of F1
  ix := Min(Trunc((x - ATableStart) / ATableStep) + 1, TableDimensionX - 1); // Min() ensures that x1 is left of x2 if x2 is last table element

  // Table1D x axis values are not stored. They can all be calculated from ATableStart and ATableStep parameters
  // but no need for finding them all. We can just find x1 which is left of x and x2 which is right of x.
  x1 := ATableStart + (ix - 1) * ATableStep;

  // get F1 and F2 array items with x in between them
  f1 := ATable1D[ix - 1];
  f2 := ATable1D[ix];

  // calculate relative distances of x from x1 and x2
  qx := (x - x1) / ATableStep;
  px := 1.0 - qx;

  // calculate Fx
  Result := px * f1 + qx * f2;
end;


function Interpolate2D(const x, y: TInterpolatedType;
                       const ATable2D: TArr2D; // dynamic array version
                       const ATableStartX, ATableStepX: TInterpolatedType;
                       const ATableStartY, ATableStepY: TInterpolatedType): TInterpolatedType;
var
  ix, iy: word;
  x1, y1: TInterpolatedType;
  px, qx, py, qy: TInterpolatedType;
  f11, f21, f12, f22, fx1, fx2: TInterpolatedType;
  TableDimensionX, TableDimensionY: word;
begin
  TableDimensionX := Length(ATable2D[0]);
  TableDimensionY := Length(ATable2D);

  ix  := Min(Trunc((x - ATableStartX) / ATableStepX) + 1, TableDimensionX - 1); // calculate column of F11
  iy  := Min(Trunc((y - ATableStartY) / ATableStepY) + 1, TableDimensionY - 1); // calculate row    of F11

  x1  := ATableStartX + (ix - 1) * ATableStepX;
  qx  := (x - x1) / ATableStepX;
  px  := 1.0 - qx;
  f11 := ATable2D[iy - 1, ix - 1];
  f21 := ATable2D[iy - 1, ix    ];
  fx1 := px * f11 + qx * f21;

  y1  := ATableStartY + (iy - 1) * ATableStepY;
  qy  := (y - y1) / ATableStepY;
  py  := 1.0 - qy;
  f12 := ATable2D[iy    , ix - 1];
  f22 := ATable2D[iy    , ix    ];
  fx2 := px * f12 + qx * f22;

  Result := py * fx1 + qy * fx2; // Fxy = Result
end;


function Interpolate3D(const x, y, z: TInterpolatedType;
                       const ATable3D: TArr3d; // dynamic array version
                       const ATableStartX, ATableStepX: TInterpolatedType;
                       const ATableStartY, ATableStepY: TInterpolatedType;
                       const ATableStartZ, ATableStepZ: TInterpolatedType): TInterpolatedType;
var
  ix, iy, iz: word;
  x1, y1, z1: TInterpolatedType;
  px, qx, py, qy, pz, qz: TInterpolatedType;
  f111, f211, f121, f221, f112, f212, f122, f222, fx11, fx21, fx12, fx22, fxy1, fxy2: TInterpolatedType;
  TableDimensionX, TableDimensionY, TableDimensionZ: word;
begin
  TableDimensionX := Length(ATable3D[0,0]);
  TableDimensionY := Length(ATable3D[0]);
  TableDimensionZ := Length(ATable3D);

  ix   := Min(Trunc((x - ATableStartX) / ATableStepX) + 1, TableDimensionX - 1); // calculate column of F111
  iy   := Min(Trunc((y - ATableStartY) / ATableStepY) + 1, TableDimensionY - 1); // calculate row    of F111
  iz   := Min(Trunc((z - ATableStartZ) / ATableStepZ) + 1, TableDimensionZ - 1); // calculate depth  of F111

  x1   := ATableStartX + (ix - 1) * ATableStepX;
  qx   := (x - x1) / ATableStepX;
  px   := 1.0 - qx;
  f111 := ATable3D[iz - 1, iy - 1, ix - 1];
  f211 := ATable3D[iz - 1, iy - 1, ix    ];
  fx11 := px * f111 + qx * f211;

  y1   := ATableStartY + (iy - 1) * ATableStepY;
  qy   := (y - y1) / ATableStepY;
  py   := 1.0 - qy;
  f121 := ATable3D[iz - 1, iy    , ix - 1];
  f221 := ATable3D[iz - 1, iy    , ix    ];
  fx21 := px * f121 + qx * f221;

  fxy1 := py * fx11 + qy * fx21;

  z1   := ATableStartZ + (iz - 1) * ATableStepZ;
  qz   := (z - z1) / ATableStepZ;
  pz   := 1.0 - qz;
  f112 := ATable3D[iz    , iy - 1, ix - 1];
  f212 := ATable3D[iz    , iy - 1, ix    ];
  fx12 := px * f112 + qx * f212;

  f122 := ATable3D[iz    , iy    , ix - 1]; // (pf + IncZ + IncY)^;
  f222 := ATable3D[iz    , iy    , ix    ]; // (pf + IncZ + IncY + 1)^;
  fx22 := px * f122 + qx * f222;

  fxy2 := py * fx12 + qy * fx22;

  Result := pz * fxy1 + qz * fxy2; // Fxyz = Result
end;


function Interpolate4D(const x, y, z, u: TInterpolatedType;
                       const ATable4D: TArr4D; // dynamic array version
                       const ATableStartX, ATableStepX: TInterpolatedType;
                       const ATableStartY, ATableStepY: TInterpolatedType;
                       const ATableStartZ, ATableStepZ: TInterpolatedType;
                       const ATableStartU, ATableStepU: TInterpolatedType): TInterpolatedType;
var
  ix, iy, iz, iu: word;
  x1, x2, f1111, f2111,                                           px, qx, fx111, fx211: TInterpolatedType;
  y1, y2, f1211, f2211,                                           py, qy, fx121, fx221: TInterpolatedType;
  z1, z2, f1121, f2121, f1221, f2221,                             pz, qz, fxy11, fxy21: TInterpolatedType;
  u1, u2, f1112, f2112, f1212, f2212, f1122, f2122, f1222, f2222, pu, qu, fx112, fx212, fx122, fx222, fxy12, fxy22, fxyz1, fxyz2: TInterpolatedType;
  TableDimensionX, TableDimensionY, TableDimensionZ, TableDimensionU: word;
begin
  TableDimensionX := Length(ATable4D[0, 0, 0]);
  TableDimensionY := Length(ATable4D[0, 0]);
  TableDimensionZ := Length(ATable4D[0]);
  TableDimensionU := Length(ATable4D);

  ix    := Min(Trunc((x - ATableStartX) / ATableStepX) + 1, TableDimensionX - 1); // calculate column of F1111
  iy    := Min(Trunc((y - ATableStartY) / ATableStepY) + 1, TableDimensionY - 1); // calculate row    of F1111
  iz    := Min(Trunc((z - ATableStartZ) / ATableStepZ) + 1, TableDimensionZ - 1); // calculate depth  of F1111
  iu    := Min(Trunc((u - ATableStartU) / ATableStepU) + 1, TableDimensionU - 1); // calculate volume of F1111

  // X axis:
  x1    := ATableStartX + (ix - 1) * ATableStepX;
  x2    := x1 + ATableStepX;
  qx    := (x - x1) / (x2 - x1);
  px    := 1.0 - qx;
  f1111 := ATable4D[iu - 1, iz - 1, iy - 1, ix - 1];
  f2111 := ATable4D[iu - 1, iz - 1, iy - 1, ix    ];
  fx111 := px * f1111 + qx * f2111;

  // Y axis:
  y1    := ATableStartY + (iy - 1) * ATableStepY;
  y2    := y1 + ATableStepY;
  qy    := (y - y1) / (y2 - y1);
  py    := 1.0 - qy;
  f1211 := ATable4D[iu - 1, iz - 1, iy    , ix - 1];
  f2211 := ATable4D[iu - 1, iz - 1, iy    , ix    ];
  fx211 := px * f1211 + qx * f2211;

  fxy11 := py * fx111 + qy * fx211;

  // Z axis:
  z1    := ATableStartZ + (iz - 1) * ATableStepZ;
  z2    := z1 + ATableStepZ;
  qz    := (z - z1) / (z2 - z1);
  pz    := 1.0 - qz;
  f1121 := ATable4D[iu - 1, iz    , iy - 1, ix - 1];
  f2121 := ATable4D[iu - 1, iz    , iy - 1, ix    ];
  fx121 := px * f1121 + qx * f2121;

  f1221 := ATable4D[iu - 1, iz    , iy    , ix - 1];
  f2221 := ATable4D[iu - 1, iz    , iy    , ix    ];
  fx221 := px * f1221 + qx * f2221;

  fxy21 := py * fx121 + qy * fx221;

  fxyz1 := pz * fxy11 + qz * fxy21;

  // U axis:
  u1    := ATableStartU + (iu - 1) * ATableStepU;
  u2    := u1 + ATableStepU;
  qu    := (u - u1) / (u2 - u1);
  pu    := 1.0 - qu;
  f1112 := ATable4D[iu    , iz - 1, iy - 1, ix - 1];
  f2112 := ATable4D[iu    , iz - 1, iy - 1, ix    ];
  fx112 := px * f1112 + qx * f2112;

  f1212 := ATable4D[iu    , iz - 1, iy    , ix - 1];
  f2212 := ATable4D[iu    , iz - 1, iy    , ix    ];
  fx212 := px * f1212 + qx * f2212;

  fxy12 := py * fx112+ qy * fx212;

  f1122 := ATable4D[iu    , iz    , iy - 1, ix - 1];
  f2122 := ATable4D[iu    , iz    , iy - 1, ix    ];
  fx122 := px * f1122 + qx * f2122;

  f1222 := ATable4D[iu    , iz    , iy    , ix - 1];
  f2222 := ATable4D[iu    , iz    , iy    , ix    ];
  fx222 := px * f1222 + qx * f2222;

  fxy22 := py * fx122+ qy * fx222;

  fxyz2 := pz * fxy12 + qz * fxy22;

  Result := pu * fxyz1 + qu * fxyz2; // Fxyzu = Result
end;


end.

